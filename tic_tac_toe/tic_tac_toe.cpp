
#include <string>
#include <array>
#include <bitset>
#include <ostream>
#include <iostream>

struct move
{
	unsigned r;
	unsigned c;
};

struct direction
{
	int r;
	int c;
};

std::ostream& operator<<(std::ostream& ostream, const move& move)
{
	ostream << "(" << move.r << ", " << move.c << ")";

	return ostream;
}

enum class piece : char
{
	x = 'X',
	o = 'O',
	empty = ' ',
	invalid = '\0'
};

std::ostream& operator<<(std::ostream& ostream, const piece& p)
{
	ostream << static_cast<char>(p);

	return ostream;
}

class grid
{
public:
	const static unsigned dimension = 3;
	const static unsigned data_size = dimension * dimension;

	static bool in_bounds(move m)
	{
		return in_bounds(m.r, m.c);
	}

	static bool in_bounds(unsigned r, unsigned c)
	{
		if (r >= dimension || c >= dimension)
		{
			return false;
		}

		return true;
	}

	static unsigned to_index(move m)
	{
		return to_index(m.r, m.c);
	}

	static unsigned to_index(unsigned r, unsigned c)
	{
		if (!in_bounds(r, c))
		{
			return -1;
		}

		return r * dimension + c;
	}

	void reset()
	{
		std::fill(data.begin(), data.end(), piece::empty);
		unoccupiedSpace = data_size;
	}

	piece get_space(move move) const
	{
		return get_space(move.r, move.c);
	}

	piece get_space(unsigned r, unsigned c) const
	{
		int index = to_index(r, c);

		if (index < data_size)
		{
			return data[index];
		}

		return piece::invalid;
	}

	void set_space(move move, piece piece)
	{
		int index = to_index(move.r, move.c);

		if (index < data_size)
		{
			if (piece != piece::empty)
			{
				unoccupiedSpace--;
			}
			else
			{
				unoccupiedSpace++;
			}

			data[index] = piece;
		}
	}

	bool is_full() const
	{
		return unoccupiedSpace == 0;
	}

private:
	std::array<piece, dimension * dimension> data;
	unsigned unoccupiedSpace;
};

class player
{
public:
	player(::piece piece) :
		piece(piece)
	{
	}

	::piece get_piece() const
	{
		return piece;
	}

	move get_move(const grid& grid)
	{
		while (true)
		{
			std::cout << "Player " << piece << " - Select your space to place piece (Row-Index Column-Index):\n";
			std::cin.clear();
			unsigned int r;
			std::cin >> r;

			char delimiter;
			std::cin.get(delimiter);

			unsigned int c;
			std::cin >> c;
			
			if (std::cin.fail())
			{
				std::cout << "Invalid entry. Try again.\n";
				continue;
			}

			move move{ r, c };

			::piece currentPiece = grid.get_space(move);
			if (currentPiece == ::piece::invalid)
			{
				std::cout << "The coordinates " << move << " are invalid. Try again.\n";
				continue;
			}

			if (currentPiece != ::piece::empty)
			{
				std::cout << "Space " << move << " is already occupied. Try again.\n";
				continue;
			}

			return move;
		}
	}
private:
	::piece piece;
};

std::ostream& operator<<(std::ostream& ostream, const grid& grid)
{
	for (int r = 0; r < grid::dimension; ++r)
	{
		if (r == 0)
		{
			ostream << ' ';
			for (int c = 0; c < grid::dimension; ++c)
			{
				ostream << c << ' ';
			}
			ostream << "\n";
		}

		ostream << r;

		for (int c = 0; c < grid::dimension; ++c)
		{
			piece p = grid.get_space(r, c);

			ostream << p;

			char delimiter = (c < (grid::dimension - 1)) ? '|' : '\n';

			ostream << delimiter;
		}

		if (r < (grid::dimension - 1))
		{
			ostream << " -+-+-\n";
		}
	}

	return ostream;
}

class game
{
public:
	game() :
		grid(),
		xPlayer(piece::x),
		oPlayer(piece::o)
	{}

	void play_game()
	{
		reset();

		std::array<player*, 2> players = { &xPlayer, &oPlayer };

		std::cout << "The grid:\n" << grid << "\n";
		while(true)
		{
			for (player* player : players)
			{
				piece p = player->get_piece();
				move m = player->get_move(grid);

				std::cout << "Player " << p << " is placing at: " << m << "\n";

				grid.set_space(m, p);

				std::cout << "Grid after " << p << " was placed:\n" << grid << "\n";

				if (evaluate_move(m))
				{
					std::cout << "Player " << p << " has won the game.\n";
					return;
				}
				else if (grid.is_full())
				{
					std::cout << "Cat's game. Nobody won.\n";
					return;
				}
			}
		}
	}
private:
	void reset()
	{
		grid.reset();
	}
	
	bool evaluate_move(move m)
	{
		piece piece = grid.get_space(m);
		
		bool result = false;
		result |= evaluate_move(m, piece, ::direction{ 1, 1 });

		unsigned countB = 1;
		result |= evaluate_move(m, piece, ::direction{ 1, 0 });
		
		unsigned countC = 1;
		result |= evaluate_move(m, piece, ::direction{ 0, 1 });
		
		unsigned countD = 1;
		result |= evaluate_move(m, piece, ::direction{ -1, 1 });

		return result;
	}

	bool evaluate_move(move m, piece piece, ::direction direction)
	{
		unsigned int count = 1;

		move mPos{ m.r + direction.r, m.c + direction.c };
		move mNeg{ m.r - direction.r, m.c - direction.c };

		while (grid::in_bounds(mPos))
		{
			if (grid.get_space(mPos) == piece)
			{
				count++;
			}

			mPos.r += direction.r;
			mPos.c += direction.c;
		};

		while (grid::in_bounds(mNeg))
		{
			if (grid.get_space(mNeg) == piece)
			{
				count++;
			}

			mNeg.r -= direction.r;
			mNeg.c -= direction.c;
		};

		return count == 3;
	}

	::grid grid;
	::player xPlayer;
	::player oPlayer;
};

int main(int argsc, const char** argsv)
{
	::game game;

	while (true)
	{
		std::cout << "Play new game (Y/N)?";
		char response;
		std::cin >> response;

		if (response == 'n' || response == 'N')
		{
			break;
		}
		else if (response == 'y' || response == 'Y')
		{
			game.play_game();
		}
	}

	return 0;
}
